package main;

import java.util.Map;
import java.util.Timer;
import java.util.HashMap;
import java.util.TimerTask;
import java.io.File;
import java.io.FileNotFoundException;

import java.io.IOException;
import java.time.LocalDateTime;

import com.github.instagram4j.instagram4j.IGClient;
import com.github.instagram4j.instagram4j.exceptions.IGLoginException;

import bots.LikeBot;
import database.LocalDatabase;
import bots.FollowBot;
import other.Constants;
import other.Useful;
import bots.ContentGeneratorBot;


public class ParlamentoBurro {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		File userPropertiesFile = new File(Constants.DIRECTORY_CONFIG);
		if(userPropertiesFile.exists() && !userPropertiesFile.isDirectory()) { 
			TimerTask fetchMemes = new Bot();
	     	Timer timer = new Timer();
	     	timer.schedule(fetchMemes, 0, 3600000);	//TODO -- CHANGE TO READ FROM FILE
		}
		else {
			LocalDatabase.createInitialConfigFile();
		}		
	}
}

class Bot extends TimerTask{
	
	@Override
	public void run() {
		Map<String, String> configFileContent = new HashMap<String, String>();
		configFileContent = LocalDatabase.readProperties(Constants.FILE_CONFIG_PATH);
		
		try {
			String[] initTimeArray = configFileContent.get(Constants.BOT_TIME_INITIATE).split(Constants.SPLITTER);
			String[] stopTimeArray = configFileContent.get(Constants.BOT_TIME_STOP).split(Constants.SPLITTER);
			
			LocalDateTime now = LocalDateTime.now();
			LocalDateTime initTime = Useful.getlocalDateTime(initTimeArray);
			LocalDateTime stopTime = Useful.getlocalDateTime(stopTimeArray);
			boolean runForever = initTime.compareTo(stopTime) == 0;
			boolean closeAfterMidnight = Integer.valueOf(stopTimeArray[0]) < Integer.valueOf(initTimeArray[0]);
			
			if(runForever ? true : closeAfterMidnight ? now.compareTo(initTime) >= 0 || now.compareTo(stopTime) <= 0 : now.compareTo(initTime) >= 0 && now.compareTo(stopTime) <= 0) {				
				File variablesFile = new File(Constants.FILE_CONFIG_PATH);
				Map<String, String> variableFileContent = new HashMap<>();
				if(variablesFile.exists() && !variablesFile.isDirectory()) {
					variableFileContent = LocalDatabase.readProperties(Constants.FILE_CONFIG_PATH);					
					String hasUnfollowedToday = variableFileContent.get("hasUnfollowedToday");
					if(!hasUnfollowedToday.equals(Constants.NO) && LocalDateTime.parse(hasUnfollowedToday).toLocalDate().compareTo(LocalDateTime.now().toLocalDate()) < 0) {
						variableFileContent.put("hasUnfollowedToday", Constants.NO);
					}
				}
				else {
					//Save settings Map in Local	
					variableFileContent = LocalDatabase.createInitialVariableFile();
				}
				
				IGClient client = IGClient.builder().username(configFileContent.get(Constants.USERNAME)).password(configFileContent.get(Constants.PASSWORD)).login();
				//Fetch and Upload Memes 
//				ContentGeneratorBot.generateContent(client, configFileContent);
				//Follow and unfollow people
				FollowBot.followUnfollow(client, configFileContent, variableFileContent);
				//Like Images
//				LikeBot.likeImages(client, configFileContent, variableFileContent, variablesProperties);
				//Comment Images
				
				//DM People
			}else{
				Useful.printTimeStampedMessage("Sorry Boss, we're off the clock! If you want me to run, just change the \"user.properties\" configuration file\nI'm currently only set to work between " + configFileContent.get(Constants.BOT_TIME_INITIATE) + " and " + configFileContent.get(Constants.BOT_TIME_STOP));
			}
		} catch (IGLoginException e) {
			Useful.printTimeStampedMessage(Constants.EXCEPTION_IG_LOGIN);
			e.printStackTrace();
		}
	}
}
