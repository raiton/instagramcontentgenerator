package main;


import java.io.File;
import java.util.Map;
import java.util.Timer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TimerTask;
import java.io.FileNotFoundException;

import java.io.IOException;
import java.time.LocalDateTime;

import com.github.instagram4j.instagram4j.IGClient;
import com.github.instagram4j.instagram4j.exceptions.IGLoginException;

import bots.LikeBot;
import database.LocalDatabase;
import externalapis.Instagram;
import bots.FollowBot;
import other.Constants;
import other.Useful;
import bots.ContentGeneratorBot;
import bots.DMBot;


public class MemeFetcher {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		File userPropertiesFile = new File(Constants.FILE_CONFIG_PATH);
		if(userPropertiesFile.exists() && !userPropertiesFile.isDirectory()) {
			Map<String, String> configFileContent = new HashMap<String, String>();
			configFileContent = LocalDatabase.readProperties(Constants.FILE_CONFIG_PATH);
			TimerTask fetchMemes = new MemeFetcherBot();
	     	Timer timer = new Timer();
	     	try {
	     		timer.schedule(fetchMemes, 0, Integer.valueOf(configFileContent.get(Constants.SECONDS_IN_BETWEEN_POSTS))*1000);	     		
	     	}catch(NumberFormatException e) {
	     		Useful.printTimeStampedMessage(Constants.FILE_CONFIG_ERROR);
	     	}
		}
		else {
			LocalDatabase.createInitialConfigFile();
		}		
	}
}

class MemeFetcherBot extends TimerTask{
	
	@Override
	public void run() {
		Map<String, String> configFileContent = LocalDatabase.readProperties(Constants.FILE_CONFIG_PATH);
		
		try {
			String[] initTimeArray = configFileContent.get(Constants.BOT_TIME_INITIATE).split(Constants.SPLITTER);
			String[] stopTimeArray = configFileContent.get(Constants.BOT_TIME_STOP).split(Constants.SPLITTER);
			
			LocalDateTime now = LocalDateTime.now();
			LocalDateTime initTime = Useful.getlocalDateTime(initTimeArray);
			LocalDateTime stopTime = Useful.getlocalDateTime(stopTimeArray);
			boolean runForever = initTime.compareTo(stopTime) == 0;
			boolean closeAfterMidnight = Integer.valueOf(stopTimeArray[0]) < Integer.valueOf(initTimeArray[0]);
			
			if(runForever ? true : closeAfterMidnight ? now.compareTo(initTime) >= 0 || now.compareTo(stopTime) <= 0 : now.compareTo(initTime) >= 0 && now.compareTo(stopTime) <= 0) {
				File variablesFile = new File(Constants.FILE_VARIABLE_PATH);
				Map<String, String> variableFileContent = new HashMap<>();
				if(variablesFile.exists() && !variablesFile.isDirectory()) {
					variableFileContent = LocalDatabase.readProperties(Constants.FILE_VARIABLE_PATH);					
					String hasUnfollowedToday = variableFileContent.get(Constants.HAS_UNFOLLOWED_TODAY);
					if(!hasUnfollowedToday.equals(Constants.NO) && LocalDateTime.parse(hasUnfollowedToday).toLocalDate().compareTo(LocalDateTime.now().toLocalDate()) < 0) {
						variableFileContent.put(Constants.HAS_UNFOLLOWED_TODAY, Constants.NO);
					}
				}
				else {
					variableFileContent = LocalDatabase.createInitialVariableFile();
				}
				
				IGClient client = Instagram.login(configFileContent);
				//Fetch and Upload Memes 
				if(configFileContent.get(Constants.BOT_POST).equals(Constants.YES)) {
					ContentGeneratorBot.generateContent(client, configFileContent);
				}
				
				//Follow and unfollow people
				if(configFileContent.get(Constants.BOT_FOLLOW_UNFOLLOW).equals(Constants.YES)) {
					FollowBot.followUnfollow(client, configFileContent, variableFileContent);
				}
				
				//Like Images
				if(configFileContent.get(Constants.BOT_LIKE).equals(Constants.YES)) {
					LikeBot.likeImages(client, configFileContent, variableFileContent);	
				}
				
				//Comment Images
				if(configFileContent.get(Constants.BOT_COMMENT).equals(Constants.YES)){

				}
				
				//DM People
				if(configFileContent.get(Constants.BOT_DM).equals(Constants.YES)){
					DMBot.execute(client, configFileContent, variableFileContent);
				}
				
			}else{
				Useful.printTimeStampedMessageWAttributes(Constants.OFF_THE_CLOCK, Arrays.asList(configFileContent.get(Constants.BOT_TIME_INITIATE), configFileContent.get(Constants.BOT_TIME_STOP)));
			}
		}catch (IGLoginException e) {
			Useful.printTimeStampedMessage(Constants.EXCEPTION_IG_LOGIN);
			e.printStackTrace();
		}catch (NullPointerException e) {
			Useful.printTimeStampedMessage(Constants.FILE_CONFIG_ERROR);
			e.printStackTrace();
		}
	}
}
