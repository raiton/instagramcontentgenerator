package main;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import com.github.instagram4j.instagram4j.IGClient;
import com.github.instagram4j.instagram4j.actions.feed.FeedIterable;
import com.github.instagram4j.instagram4j.actions.users.UserAction;
import com.github.instagram4j.instagram4j.exceptions.IGLoginException;
import com.github.instagram4j.instagram4j.models.user.Profile;
import com.github.instagram4j.instagram4j.requests.friendships.FriendshipsFeedsRequest;
import com.github.instagram4j.instagram4j.responses.feed.FeedUsersResponse;

import database.LocalDatabase;
import externalapis.Instagram;
import other.Constants;
import other.Useful;

public class RunScript {
	public static void main(String[] args) {
//		addFollowersToMap();
		checkOldestFollowers();
	}
	
	
	private static void checkOldestFollowers() { 
		Map<String, String> configFileContent = LocalDatabase.readProperties(Constants.FILE_CONFIG_PATH);
		IGClient client = null;
		try {
//			client = Instagram.login(configFileContent);
			client = Instagram.login("parlamentoburro","COiso124!!");
		} catch (IGLoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int currentUnfollowCount = 0;
		int timeToSleep = Integer.valueOf(configFileContent.get(Constants.SECONDS_IN_BETWEEN_FOLLOWS));
		int maxDailyFollowUnfollow = Integer.valueOf(configFileContent.get(Constants.MAX_DAILY_FOLLOW_UNFOLLOW));
		String waitingString = Useful.getWatitingString(timeToSleep);
						
		//Fetch or create a follows.properties file that stores people you've followed, and save that info in "followsMap"
		Map<String, String> followsMap = LocalDatabase.readProperties(Constants.FILE_FOLLOWS_PATH);
				
		//For each person in followsMap unfollow up to maxDailyFollowUnfollowCount if x days have passed && hasn't followed back
		for(Entry<String, String> profile: followsMap.entrySet()) {
				String name = profile.getKey();
				// If hasn't been unfollowed already or follows you back
				if(!followsMap.get(name).equals(Constants.UNFOLLOWED) && !followsMap.get(name).equals(Constants.FOLLOWED_BACK)) {
					int daysFollowing = LocalDate.now().compareTo(LocalDateTime.parse(followsMap.get(name)).toLocalDate());
					
						if(daysFollowing >= Integer.valueOf(configFileContent.get("unfollowPeopleWhoHaventFollowedInXDays"))) {
							UserAction userToUnfollow = Instagram.getUser(client, name);
							boolean followsBack = userToUnfollow.getFriendship().join().isFollowed_by();
							if(!followsBack) {
								Useful.printTimeStampedMessage("You've been following " + userToUnfollow.getUser().getFull_name() + " for " +  daysFollowing + " days, and he hasn't followed you back.");
								
								//Save Map in Local File
								LocalDatabase.saveProperties(followsMap, Constants.FILE_FOLLOWS_PATH);		
							}else {
								Useful.printTimeStampedMessage(userToUnfollow.getUser().getFull_name() + " follows you so there's no problem that you've been following him for " +  daysFollowing);
								//Save that this account has followed you back
								followsMap.put(name, Constants.FOLLOWED_BACK);
								LocalDatabase.saveProperties(followsMap, Constants.FILE_FOLLOWS_PATH);
							}
						}		
				}
			}		
	}


	private static void addFollowersToMap() {
		Map<String, String> configFileContent = new HashMap<String, String>();
		configFileContent = LocalDatabase.readProperties(Constants.FILE_CONFIG_PATH);
		IGClient client = null;
		try {
			client = Instagram.login(configFileContent);
		} catch (IGLoginException e) {
			e.printStackTrace();
		}
		Map<String, LocalDateTime>  followMap = LocalDatabase.readFollowProperties();
		UserAction user = Instagram.getUser(client, configFileContent.get(Constants.USERNAME));			    
		FeedIterable<FriendshipsFeedsRequest, FeedUsersResponse> followingFeed = user.followingFeed();
		List<Profile> followingList = followingFeed.iterator().next().getUsers();
		LocalDateTime now = LocalDateTime.now();
		System.out.println(followingList.size());
		for(Profile profile: followingList) {
			String userName = profile.getUsername();
//			UserAction userToFollow = Instagram.getUser(client, userName);
//			Friendship friendshipWithUserToFollow = userToFollow.getFriendship().join();
			if(!followMap.containsKey(userName)) {
				followMap.put(userName, now);		
			}	
		}
		LocalDatabase.saveProperties(followMap, Constants.FILE_FOLLOWS_PATH);
		System.out.println(followMap.toString());
		System.out.println("done");
	}
	
//	/**
//	 * Follow users that follow the accounts provided on the config file
//	 * @param client
//	 * @param configFileContent
//	 * @param variableFileContent
//	 * @param variablesProperties
//	 */
//	private static void followUsers(IGClient client, Map<String, String> configFileContent, Map<String, String> variableFileContent) {		
//		List<String> usersToFollowFrom = new ArrayList<>(Arrays.asList(configFileContent.get(Constants.ACCOUNTS_TO_FOLLOW_FROM).split(Constants.SPLITTER)));
//		int maxDailyFollowUnfollow = Integer.valueOf(configFileContent.get(Constants.MAX_DAILY_FOLLOW_UNFOLLOW));
//		int followXPeopleInARow = getUsersPerCicle(configFileContent);
//		
//		for(String userToFollowFrom: usersToFollowFrom) {
//			Properties followsProperties = new Properties();
//			Map<String, LocalDateTime> followsMap = LocalDatabase.readFollowProperties(followsProperties);
//			UserAction user = client.actions().users().findByUsername(userToFollowFrom).join();			    
//			FeedIterable<FriendshipsFeedsRequest, FeedUsersResponse> followersFeed = user.followersFeed();		
//			
//			int i = 0;
//			int timeToSleep = Integer.valueOf(configFileContent.get(Constants.SECONDS_IN_BETWEEN_FOLLOWS));
//			String waitingString = timeToSleep < 60 ? "Waiting for " + timeToSleep + " seconds..." : "Waiting for " + timeToSleep/60 + " minutes...";
//			//Followers
//			List<Profile> followerList = followersFeed.iterator().next().getUsers(); //TODO - Only brings 99?
//			int currentFollowCount = Integer.valueOf(variableFileContent.get(Constants.CURRENT_FOLLOW_UNFOLLOW_COUNT));
//			for(Profile profile: followerList) {
//				if(currentFollowCount < maxDailyFollowUnfollow) {
//					String name = profile.getUsername();
//					try {
//						if(!followsMap.containsKey(name)) {
//							UserAction userToFollow = client.actions().users().findByUsername(name).join();
//							Friendship friendshipWithUserToFollow = userToFollow.getFriendship().join();
//							if(!friendshipWithUserToFollow.isFollowing()) {
//								userToFollow.action(FriendshipsAction.CREATE).join();
//								followsMap.put(name, LocalDateTime.now());
//								Useful.printTimeStampedMessage("You\'re now following " + userToFollow.getUser().getFull_name() + ". username: " + userToFollow.getUser().getUsername() + " who has " + userToFollow.getUser().getFollower_count() + " followers and follows " + userToFollow.getUser().getFollowing_count() + " people. An account with " + userToFollow.getUser().getMedia_count() + " posts.");
//								
//								//Add counter by 1
//								i++;
//								currentFollowCount++;
//								variableFileContent.put(Constants.CURRENT_FOLLOW_UNFOLLOW_COUNT, String.valueOf(currentFollowCount));
//								
//								//Store locals
//								LocalDatabase.saveProperties(variableFileContent, Constants.FILE_VARIABLE); 
//								LocalDatabase.saveProperties(followsMap, Constants.FILE_FOLLOWS); 
//								
//							}else {
//								Useful.printTimeStampedMessage("You were already following " + userToFollow.getUser().getFull_name());
//							}
//						}else {
//							Useful.printTimeStampedMessage("You\'re already following " + name);
//						}
//					}catch (Exception e) {
//						if(e.getCause().getLocalizedMessage().equals("User not found")) { //TODO - FIX THIS EQUALS
//							Useful.printTimeStampedMessage("ERROR - USER NOT FOUND - IGResponseException - User " + name + " account appears to be gone missing. Can't Follow A Missing Person, call the Feds!");
//						}else {
//							Useful.printTimeStampedMessage(Constants.EXCEPTION_IG_RESPONSE);
//							e.printStackTrace();
//						}
//					}
//				}
//				if(i < followXPeopleInARow) {
//					Useful.sleep(timeToSleep, waitingString);
//				}else {    	
//					Useful.printTimeStampedMessage(Constants.WAITING_FOR_NEXT_CYCLE);
//					i = 0;	    	
//					break;
//				}
//			}
//		}
//	}
	
}

