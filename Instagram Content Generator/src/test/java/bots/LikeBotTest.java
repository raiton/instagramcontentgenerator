package test.java.bots;

import static org.junit.Assert.assertEquals;

import java.util.Map;
import java.util.HashMap;
import java.util.Properties;

import java.io.PrintStream;
import java.time.LocalDateTime;
import java.io.ByteArrayOutputStream;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import com.github.instagram4j.instagram4j.exceptions.IGLoginException;

import bots.LikeBot;
import other.Constants;

public class LikeBotTest {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;
	private final PrintStream originalErr = System.err;
	
	@Before
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}

	@After
	public void restoreStreams() {
	    System.setOut(originalOut);
	    System.setErr(originalErr);
	}
	
	
	@Test
	public void likeImagesEnoughLikesForTodayTest() throws IGLoginException{
		String maxDailyLikes = "200";
		Map<String, String> configFileContent = getMockConfigContent(maxDailyLikes);		
		Map<String, String> variableFileContent = getMockVariableContent(maxDailyLikes);
		LikeBot.likeImages(null, configFileContent, variableFileContent);
		assertEquals(Constants.DATE_TIME_FORMAT.format(LocalDateTime.now()) + " - Seems Like you've already liked " + maxDailyLikes + " photos today. Change the \"user.properties\" file if you want to like more more.", outContent.toString().trim());
	}

	
//	@Test
//	public void likeImagesCanStillLikeTodayTest() throws IGLoginException{
//		String actualLikes = "199";
//		String maxDailyLikes = "200";
//		Map<String, String> configFileContent = getMockConfigContent(maxDailyLikes);
//		Map<String, String> variableFileContent = getMockVariableContent(actualLikes);
//		Properties variablesProperties = getMockVariableProperties(maxDailyLikes);
//		LikeBot.likeImages(null, configFileContent, variableFileContent, variablesProperties);
//		assertEquals(Constants.DATE_TIME_FORMAT.format(LocalDateTime.now()) + " - Seems Like you've already liked " + maxDailyLikes + " photos today. Change the \"user.properties\" file if you want to like more more.", outContent.toString().trim());
//	}
	
	private Map<String, String> getMockConfigContent(String maxDailyLikes) {
		Map<String, String> map = new HashMap<String, String>();
		map.put(Constants.USERNAME, Constants.USERNAME);
		map.put(Constants.PASSWORD, Constants.PASSWORD);
		map.put("maxDailyLikes", maxDailyLikes);
		return map;
	}
	
	private Map<String, String> getMockVariableContent(String maxDailyLikes) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("currentDailyLikes", maxDailyLikes);
		return map;
	}
	
}
