package test.java.other;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.File;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;

import other.Useful;
import other.Constants;

public class UsefulTest {
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;
	private final PrintStream originalErr = System.err;
	
	@Before
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}

	@After
	public void restoreStreams() {
	    System.setOut(originalOut);
	    System.setErr(originalErr);
	}
	
	
	@Test
	public void getlocalDateTimeTrueTest() {
		String[] time = "10:30".split(Constants.SPLITTER);
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime expected = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), Integer.valueOf(time[0]), Integer.valueOf(time[1]));
		String[] initTimeArray = time;
		LocalDateTime actual = Useful.getlocalDateTime(initTimeArray);
		assertEquals(expected, actual);
	}
	
	@Test
	public void getlocalDateTimeFalseTest() {
		String[] time = "10:30".split(Constants.SPLITTER);
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime expected = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 11, Integer.valueOf(time[1]));
		String[] initTimeArray = time;
		LocalDateTime actual = Useful.getlocalDateTime(initTimeArray);
		assertNotEquals(expected, actual);
	}
		
	@Test
	public void printTimeStampedMessageTestTrue() {
		Useful.printTimeStampedMessage("Testing this function");
		assertEquals(Constants.DATE_TIME_FORMAT.format(LocalDateTime.now()) + " - " + "Testing this function".trim(), outContent.toString().trim());
	}
	
	@Test
	public void printTimeStampedMessageTestFalse() {
		Useful.printTimeStampedMessage("Testing this function");
		assertNotEquals(Constants.DATE_TIME_FORMAT.format(LocalDateTime.now()) + " - " + "Testing this function works".trim(), outContent.toString().trim());
	}

	@Test
	public void getFileNameTrueTest() {
		File file = new File("C:\\src\\main\\thisfile.jpg");
		String[] path = file.toString().split("\\\\");
		String expected = path[path.length-1].split("[.]")[0];
		String actual = Useful.getFileName(file);
		assertEquals(expected, actual);
	}
	
	@Test
	public void getFileNameFalseTest() {
		File file = new File("C:\\src\\main\\thisfile.jpg");
		String[] path = file.toString().split("\\\\");
		String expected = path[path.length-1];
		String actual = Useful.getFileName(file);
		assertNotEquals(expected, actual);
	} 

	@Test
	public void sleepTruePrintTest() {
		int seconds = 2;
		Useful.sleep(seconds, "Testing this function");
		assertEquals(Constants.DATE_TIME_FORMAT.format(LocalDateTime.now().minusSeconds(seconds)) + " - " + "Testing this function".trim(), outContent.toString().trim());
	}
	
	@Test
	public void sleepFalsePrintTest() {
		int seconds = 2;
		Useful.sleep(seconds, "Testing this function");
		assertNotEquals(Constants.DATE_TIME_FORMAT.format(LocalDateTime.now().minusSeconds(seconds-1)) + " - " + "Testing this function".trim(), outContent.toString().trim());
	}
}
