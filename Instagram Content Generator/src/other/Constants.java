package other;

import java.time.format.DateTimeFormatter;

public class Constants {
	
	//FORMATERS
	public static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	
	
	//CONSTANTS
	public static final String NO = "N";
	public static final String YES = "Y";
	public static final String ZERO = "0";
	public static final String SPLITTER = ":";
	public static final String UNFOLLOWED = "U";
	public static final String FOLLOWED_BACK = "FB";
	public static final String PLACEHOLDER = "%s";	
	public static final String REGEX_FILE_NAME = "[^a-zA-Z0-9\\s+.!]";
	
	//VARIABLES
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String CURRENT_DAILY_LIKES = "currentDailyLikes";
	public static final String HAS_UNFOLLOWED_TODAY = "hasUnfollowedToday";
	public static final String MAX_DAILY_FOLLOW_UNFOLLOW = "maxDailyFollowUnfollow";
	public static final String CURRENT_FOLLOW_UNFOLLOW_COUNT = "currentFollowUnfollowCount";
	public static final String SECONDS_IN_BETWEEN_POSTS = "secondsInBetweenPosts";
	public static final String SECONDS_IN_BETWEEN_FOLLOWS = "secondsInBetweenFollows";
	public static final String BOT_TIME_INITIATE = "initTime";
	public static final String BOT_TIME_STOP = "stopTime";
	public static final String BOT_DM = "activateDmBot";
	public static final String BOT_POST = "activatePostBot";
	public static final String BOT_LIKE = "activateLikeBot";
	public static final String BOT_COMMENT = "activateCommentBot";
	public static final String BOT_FOLLOW_UNFOLLOW = "activateFollowUnfollowBot";
	public static final String ACCOUNTS_TO_FOLLOW_FROM = "accountsToFollowFrom";
	public static final String DUMMY_ACCOUNTS = "dummyAccountsToDMFrom";
	public static final String DUMMY_PASSWORDS = "dummyAccountsPasswords";
	public static final String NUMBER_OF_POSTS_TO_KEEP_IN_STORAGE = "numberOfMemesToKeepInStorage";
	
	
	//FILES
	public static final String DIRECTORY_HOME = System.getProperty("user.dir");
	public static final String DIRECTORY_MEMES = DIRECTORY_HOME + "\\memes\\";
	public static final String DIRECTORY_CONFIG = DIRECTORY_HOME + "\\config\\";
	public static final String FILE_CONFIG = "user.properties";
	public static final String FILE_VARIABLE = "variables.properties";
	public static final String FILE_FOLLOWS = "follows.properties";
	public static final String FILE_VARIABLE_PATH = DIRECTORY_CONFIG + FILE_VARIABLE;
	public static final String FILE_FOLLOWS_PATH = DIRECTORY_CONFIG + FILE_FOLLOWS;
	public static final String FILE_CONFIG_PATH = DIRECTORY_CONFIG + FILE_CONFIG;
	public static final String FILE_DOWNLOAD_HISTORY = DIRECTORY_CONFIG + "history.properties";
	
	
	//EXCEPTIONS
	public static final String EXCEPTION_IO = "IOException: ";
	public static final String EXCEPTION_PROTOCOL = "ProtocolException: "; 	
	public static final String EXCEPTION_EXECUTION = "ExecutionException: ";
	public static final String EXCEPTION_INTERRUPTED = "InterruptedException: ";
	public static final String EXCEPTION_MALFORMED_URL = "MalformedURLException: ";
	public static final String EXCEPTION_FILE_NOT_FOUND = "FileNotFoundException: ";
	public static final String EXCEPTION_IG_LOGIN = "IGLoginException: ";
	public static final String EXCEPTION_IG_RESPONSE = "IGResponseException: ";
	public static final String EXCEPTION_IG_TRY_AGAIN_LATER = "We need to wait for a minute, Instagram is not liking all our requests";
	
	
	//PHRASES
	public static final String USER_NOT_FOUND = "User not found";
	public static final String WAIT = "Let's wait a couple seconds";
	public static final String AHH_SHIT = "Ahh Shit, here we go again";
	public static final String NO_MORE_PEOPLE = "No more people to Like";
	public static final String NO_MORE_UNFOLLOWS = "Seems like you've already unfollowed " + PLACEHOLDER + " users. That's your maximum according to the \"" + FILE_CONFIG + "\"  file.";
	public static final String WAITING_FOR_NEXT_CYCLE = "Waiting for next cycle";
	public static final String PLEASE_WAIT = "Please wait a few minutes before you try again.";
	public static final String CURRENT_FILE_COUNT = "We currently have " + PLACEHOLDER + " memes!";
	public static final String PHOTO_DELETE_FAIL = "Failed to delete the photo from local storage";
	public static final String PHOTO_DELETE_SUCCESS = "Photo deleted from local storage successfully";
	public static final String PHOTO_LIKED = "You've Liked " + PLACEHOLDER + "'s photo: " + PLACEHOLDER;
	public static final String ENOUGH_UNFOLLOWED = "Seems Like you've already unfollowed enough people for today";
	public static final String ENOUGH_POSTS = "we have more than " + PLACEHOLDER + " memes! We have " + PLACEHOLDER + " memes!";
	public static final String ENOUGH_FOLLOWS = "Seems Like you've already followed " + PLACEHOLDER + " people today. Change the \"user.properties\" file if you want to follow more.";
	public static final String MESSAGE_DEFAULT_FOLLOW_NO_USERS = "Seems Like you don't want to follow or unfollow people. If you do, edit the \"" + FILE_CONFIG + "\" file inside the config folder to start.";
	public static final String FIRST_RUN = "Seems like this is the first time you're running the bot, edit the \"" + FILE_CONFIG + "\" file inside the config folder to start.";
	public static final String FILE_CONFIG_ERROR = "There's an error with your file \"" + FILE_CONFIG + "\". Either fix it or delete and reconfigure it";
	public static final String OFF_THE_CLOCK = "Sorry Boss, we're off the clock! If you want me to run, just change the \"user.properties\" configuration file\nI'm currently only set to work between " + PLACEHOLDER + " and " + PLACEHOLDER;
	public static final String LIKED_MAX = "Seems Like you've already liked " + PLACEHOLDER + " photos today. Change the \"" + FILE_CONFIG + "\" file if you want to like more more.";
	public static final String FOLLOWED = "You\'re now following " + PLACEHOLDER + ". username: " + PLACEHOLDER + " who has " + PLACEHOLDER + " followers and follows " + PLACEHOLDER + " people. An account with " + PLACEHOLDER + " posts.";
	public static final String DELETE_USER_NOT_FOUND = EXCEPTION_IG_RESPONSE + " - User " + PLACEHOLDER + " account appears to be gone missing. Can't Follow A Missing Person, call the Feds!";
}
