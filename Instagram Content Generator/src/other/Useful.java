package other;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Useful {

	/**
	 * Creates a LocalDateTime with a given Hour and Minute
	 * @param timeArray - An Array with [hour,minute]
	 * @return LocalDateTime
	 */
	public static LocalDateTime getlocalDateTime(String[] timeArray) {
		LocalDateTime now = LocalDateTime.now();
		return LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), Integer.valueOf(timeArray[0]), Integer.valueOf(timeArray[1]));
	}
	
	
	/**
	 * Prints in the console a given messaging adding the current time stamp
	 * @param message
	 */
	public static void printTimeStampedMessage(String message) {
		System.out.println(Constants.DATE_TIME_FORMAT.format(LocalDateTime.now()) + " - " + message);
	}
	
	/**
	 * Prints in the console a given messaging adding the current time stamp and given replacements
	 * @param message
	 * @param replacements
	 */
	public static void printTimeStampedMessageWAttributes(String message, List<?> replacements) {
		for(Object replacement: replacements) {
			message = message.replaceFirst(Constants.PLACEHOLDER, String.valueOf(replacement));
		}
		printTimeStampedMessage(message);
	}

	/**
	 * Returns the filename of a given file, without the extension 
	 * @param file
	 * @return String with file name
	 */
	public static String getFileName(File file) {
		String[] directory = file.toString().split("\\\\");
		List<String> nameArray = new ArrayList<>(Arrays.asList(directory[directory.length-1].split("[.]")));
		nameArray.remove(nameArray.size()-1);
		String name = "";
		for(String s: nameArray) {
			name = name.concat(s);
		}
		return name;
	}

	/**
	 * Sleeps for x given seconds and prints out the given message
	 * @param howManySecondsToSleep
	 * @param message
	 */
	public static void sleep(int howManySecondsToSleep, String message) {
		if(message != null) {
			printTimeStampedMessage(message);
		}
		try {
			TimeUnit.SECONDS.sleep(howManySecondsToSleep);
		} catch (InterruptedException e) {
			printTimeStampedMessage(Constants.EXCEPTION_INTERRUPTED);
			e.printStackTrace();							
		}
	}


	/**
	 * Returns a message explaining how long the Bot is going to wait for, depending on the input
	 * @param timeToSleep
	 * @return
	 */
	public static String getWatitingString(int timeToSleep) {
		return timeToSleep < 60 ? "Waiting for " + timeToSleep + " seconds..." : timeToSleep == 60 ? "Waiting for 1 minute..." : "Waiting for " + timeToSleep/60 + " minutes...";
	}

}
