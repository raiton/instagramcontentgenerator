package bots;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.github.instagram4j.instagram4j.IGClient;
import com.github.instagram4j.instagram4j.exceptions.IGLoginException;

import externalapis.Instagram;
import other.Constants;
import other.Useful;

public class DMBot {

	public static void execute(IGClient client, Map<String, String> configFileContent, Map<String, String> variableFileContent) {				
		sendDMs(client, configFileContent, variableFileContent);	
	}
	
	private static void sendDMs(IGClient client, Map<String, String> configFileContent, Map<String, String> variableFileContent) {
		List<String> accounts = new ArrayList<>(Arrays.asList(configFileContent.get(Constants.DUMMY_ACCOUNTS)));	
		List<String> passwords = new ArrayList<>(Arrays.asList(configFileContent.get(Constants.DUMMY_PASSWORDS)));
		
		 Random rand = new Random(); 
	     int account = rand.nextInt(accounts.size()); 
	     
	     try {
			Instagram.login(accounts.get(account), passwords.get(account));
			
			//Actually DM
			
		} catch (IGLoginException e) {
			Useful.printTimeStampedMessage(Constants.EXCEPTION_IG_LOGIN);
			e.printStackTrace();
			Useful.sleep(2, Constants.EXCEPTION_IG_TRY_AGAIN_LATER);
			sendDMs(client, configFileContent, variableFileContent);
		}
	}
}
