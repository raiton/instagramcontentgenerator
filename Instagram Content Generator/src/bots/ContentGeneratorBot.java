package bots;

import java.net.URL;
import java.io.File;
import java.util.Map;
import java.awt.Image;
import java.util.Random;
import java.util.Arrays;
import java.util.HashMap;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map.Entry;
import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;

import javax.imageio.ImageIO;

import com.github.instagram4j.instagram4j.IGClient;

import database.LocalDatabase;
import externalapis.Instagram;
import externalapis.MemeAPI;
import other.Constants;
import other.MediaManager;
import other.Useful;

public class ContentGeneratorBot {
	
	public static void generateContent(IGClient client, Map<String, String> configFileContent) {
			
		int numberOfMemesToFetch = Integer.parseInt(configFileContent.get(Constants.NUMBER_OF_POSTS_TO_KEEP_IN_STORAGE));		
				
		//Fetches Memes if needed
		int numberOfFiles = new File(Constants.DIRECTORY_MEMES).listFiles().length;
		if(numberOfFiles > numberOfMemesToFetch) {
			Useful.printTimeStampedMessageWAttributes(Constants.ENOUGH_POSTS, Arrays.asList(numberOfMemesToFetch, numberOfFiles));
		}else {
			fetchMemes(numberOfMemesToFetch);
		}
		
		//Posts a meme to IG
		postMemesToIg(client);
	}
			
	private static void postMemesToIg(IGClient client) {
			
		Random rand = new Random();
		
		File memesDirectoryDir = new File(Constants.DIRECTORY_MEMES);
		File[] files = memesDirectoryDir.listFiles();
	
		File fileToUpload = files[rand.nextInt(files.length)];
		Useful.printTimeStampedMessage("Uploading " + fileToUpload);
		
		Instagram.uploadPhoto(client, fileToUpload);
	}

	private static void fetchMemes(int numberOfMemes) {	
		String response = MemeAPI.getMemes(numberOfMemes);		
		String[] array = response.split(",");
		//<Link, Title>
		Map<String, String> map = new HashMap<>();
		
		Map<String, String> ldapContent = new HashMap<String, String>();
			
			
		File dataFile = new File(Constants.FILE_DOWNLOAD_HISTORY);
			
		if(dataFile.exists() && !dataFile.isDirectory()) { 
			ldapContent = LocalDatabase.readProperties(Constants.FILE_DOWNLOAD_HISTORY);
		}
				
			for(int i = 0 ; i < array.length; i++) {
				if(array[i].startsWith("\"title")) {
					if(!ldapContent.containsKey(array[i+1])) {
						map.put(array[i+1], array[i].split("\"title\":")[1]);						
					}
				}
			}
			
			//Save Map in Local
			LocalDatabase.saveProperties(map, Constants.FILE_DOWNLOAD_HISTORY);
			
			//Run map
			for(Entry<String, String> entry  : map.entrySet()) {
				String memeLink = entry.getKey().length() >= 7 ? entry.getKey().substring(7).replaceAll("\"", "") : "";
				try(InputStream in = new URL(memeLink).openStream()){
					String type = memeLink.substring(memeLink.length()-4);
					if(type == ".jpg" || type == ".gif") {//TODO - SAME NAME IS BEING REPLACED
						Files.copy(in, Paths.get(Constants.DIRECTORY_MEMES + entry.getValue().replaceAll(Constants.REGEX_FILE_NAME, "") + type));						
					}else {//TODO - SAME NAME IS BEING REPLACED
						Image image = ImageIO.read(in);
						BufferedImage bi = MediaManager.createResizedCopy(image, 1000, 1000, true);
						ImageIO.write(bi, "jpg", new File(Constants.DIRECTORY_MEMES + entry.getValue().replaceAll(Constants.REGEX_FILE_NAME, "") + ".jpg"));
					}
				} catch (FileAlreadyExistsException e) {
					InputStream in = null;
					try {
						in = new URL(memeLink).openStream();
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					try {
						Files.copy(in, Paths.get(Constants.DIRECTORY_MEMES + entry.getValue().replaceAll(Constants.REGEX_FILE_NAME, "") + Math.random()*500000 + memeLink.substring(memeLink.length()-4)));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				catch (MalformedURLException e) {
					Useful.printTimeStampedMessage(Constants.EXCEPTION_MALFORMED_URL);
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			int numberOfFiles = new File(Constants.DIRECTORY_MEMES).listFiles().length;
			if(numberOfFiles >= numberOfMemes) {
				Useful.printTimeStampedMessageWAttributes(Constants.CURRENT_FILE_COUNT, Arrays.asList(numberOfFiles));
			}else {
				fetchMemes(numberOfMemes-numberOfFiles);
			}
	}
}
