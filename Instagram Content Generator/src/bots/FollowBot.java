package bots;

import java.time.LocalDate;
import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.time.LocalDateTime;

import com.github.instagram4j.instagram4j.IGClient;
import com.github.instagram4j.instagram4j.models.user.Profile;
import com.github.instagram4j.instagram4j.models.friendships.Friendship;
import com.github.instagram4j.instagram4j.actions.users.UserAction;

import database.LocalDatabase;
import externalapis.Instagram;
import other.Constants;
import other.Useful;

public class FollowBot {

	/**
	 * Initiates the Follow Bot and the Unfollow Bot
	 * @param client
	 * @param configFileContent
	 * @param variableFileContent
	 * @param variablesProperties
	 */
	public static void followUnfollow(IGClient client, Map<String, String> configFileContent, Map<String, String> variableFileContent) {		
			
		String hasUnfollowedToday = variableFileContent.get(Constants.HAS_UNFOLLOWED_TODAY);	
		
		if(hasUnfollowedToday.equals(Constants.NO)) {
			unfollowUsers(client, configFileContent, variableFileContent);			
		}else {
			Useful.printTimeStampedMessage(Constants.ENOUGH_UNFOLLOWED);
		}
		
		followUsers(client, configFileContent, variableFileContent);	
	}
		
	/**
	 * Follow users that follow the accounts provided on the config file
	 * @param client
	 * @param configFileContent
	 * @param variableFileContent
	 * @param variablesProperties
	 */
	private static void followUsers(IGClient client, Map<String, String> configFileContent, Map<String, String> variableFileContent) {	
		int currentCicleFollowCount = 0;
		int maxDailyFollowUnfollow = Integer.valueOf(configFileContent.get(Constants.MAX_DAILY_FOLLOW_UNFOLLOW));
		int todayFollowCount = Integer.valueOf(variableFileContent.get(Constants.CURRENT_FOLLOW_UNFOLLOW_COUNT));
		
		List<String> usersToFollowFrom = new ArrayList<>(Arrays.asList(configFileContent.get(Constants.ACCOUNTS_TO_FOLLOW_FROM).split(Constants.SPLITTER)));
		int numberOfUsersToFollowThisCicle = getUsersPerCicle(configFileContent);
		Map<String, LocalDateTime> followsMap = LocalDatabase.readFollowProperties();
		int timeToSleep = Integer.valueOf(configFileContent.get(Constants.SECONDS_IN_BETWEEN_FOLLOWS));
		String waitingString = Useful.getWatitingString(timeToSleep);
			
			for(String userToFollowFrom: usersToFollowFrom) {
				
				List<Profile> followerList = Instagram.getFollowersFromUser(client, userToFollowFrom, maxDailyFollowUnfollow);
				
				if(todayFollowCount < maxDailyFollowUnfollow) {
					for(Profile profile: followerList) {
						String userName = profile.getUsername();
						try {
							if(!followsMap.containsKey(userName)) {
								UserAction userToFollow = Instagram.getUser(client, userName);
								Friendship friendshipWithUserToFollow = userToFollow.getFriendship().join();
								if(!friendshipWithUserToFollow.isFollowing()) {
									Instagram.follow(userToFollow);
									followsMap.put(userName, LocalDateTime.now());
									Useful.printTimeStampedMessageWAttributes(Constants.FOLLOWED, Arrays.asList(userToFollow.getUser().getFull_name(), userName, userToFollow.getUser().getFollower_count(), userToFollow.getUser().getFollowing_count(), userToFollow.getUser().getMedia_count()));
									
									//Add counter by 1
									currentCicleFollowCount++;
									todayFollowCount++;
									variableFileContent.put(Constants.CURRENT_FOLLOW_UNFOLLOW_COUNT, String.valueOf(todayFollowCount));
									Useful.printTimeStampedMessage("-----------> LOG: Current follows: " + todayFollowCount);
										
									//Store locals
									LocalDatabase.saveProperties(variableFileContent, Constants.FILE_VARIABLE_PATH); 
									LocalDatabase.saveProperties(followsMap, Constants.FILE_FOLLOWS_PATH);
								}
								if(currentCicleFollowCount < numberOfUsersToFollowThisCicle) {
									Useful.sleep(timeToSleep, waitingString);
								}else {    	
									Useful.printTimeStampedMessage(Constants.WAITING_FOR_NEXT_CYCLE);
									currentCicleFollowCount = 0;	    	
									break;
								}
							}
						}catch (Exception e) {
							manageFollowExceptions(e, userName);
						}
					}
				}else {    	    	
					break;
				}
			}
	}
		
		
	/**
	 * Unfollows users
	 * @param client
	 * @param configFileContent
	 * @param variableFileContent
	 */
	private static void unfollowUsers(IGClient client, Map<String, String> configFileContent, Map<String, String> variableFileContent) {	
		int currentUnfollowCount = 0;
		int timeToSleep = Integer.valueOf(configFileContent.get(Constants.SECONDS_IN_BETWEEN_FOLLOWS));
		int maxDailyFollowUnfollow = Integer.valueOf(configFileContent.get(Constants.MAX_DAILY_FOLLOW_UNFOLLOW));
		String waitingString = Useful.getWatitingString(timeToSleep);
			
		//Save that has unfollowed today
		saveUnfollowedToday(variableFileContent);
			
		//Fetch or create a follows.properties file that stores people you've followed, and save that info in "followsMap"
		Map<String, String> followsMap = LocalDatabase.readProperties(Constants.FILE_FOLLOWS_PATH);
				
		//For each person in followsMap unfollow up to maxDailyFollowUnfollowCount if x days have passed && hasn't followed back
		for(Entry<String, String> profile: followsMap.entrySet()) {
				String name = profile.getKey();
				// If hasn't been unfollowed already or follows you back
				if(!followsMap.get(name).equals(Constants.UNFOLLOWED) && !followsMap.get(name).equals(Constants.FOLLOWED_BACK)) {
					int daysFollowing = LocalDate.now().compareTo(LocalDateTime.parse(followsMap.get(name)).toLocalDate());
					try {
						if(daysFollowing >= Integer.valueOf(configFileContent.get("unfollowPeopleWhoHaventFollowedInXDays"))) {
							UserAction userToUnfollow = Instagram.getUser(client, name);
							boolean followsBack = userToUnfollow.getFriendship().join().isFollowed_by();
							if(!followsBack) {
								Instagram.unfollow(userToUnfollow);
								followsMap.put(name, Constants.UNFOLLOWED);
								Useful.printTimeStampedMessage("You unfollowed " + userToUnfollow.getUser().getFull_name() + " who has " + userToUnfollow.getUser().getFollower_count() + " followers and follows " + userToUnfollow.getUser().getFollowing_count() + " people. An account with " + userToUnfollow.getUser().getMedia_count() + " posts.");
								
								//Save Map in Local File
								LocalDatabase.saveProperties(followsMap, Constants.FILE_FOLLOWS_PATH);
								
								if(currentUnfollowCount < maxDailyFollowUnfollow) {
									Useful.sleep(timeToSleep, waitingString);
									currentUnfollowCount++;
								}else {
									Useful.printTimeStampedMessageWAttributes(Constants.NO_MORE_UNFOLLOWS, Arrays.asList(String.valueOf(maxDailyFollowUnfollow)));	    	
									break;
								}				
							}else {
								Useful.printTimeStampedMessage(name + " wasn't unfollowed because this account has followed you back!");
								//Save that this account has followed you back
								followsMap.put(name, Constants.FOLLOWED_BACK);
								LocalDatabase.saveProperties(followsMap, Constants.FILE_FOLLOWS_PATH);
							}
						}
					}catch(Exception e) {
						manageUnfollowExceptions(e, name, followsMap, timeToSleep);
					}
				}
			}
			Useful.printTimeStampedMessage(Constants.ENOUGH_UNFOLLOWED);
		}
		

		/**
		 * Will save in the variable file the current date as memory that unfollow bot was ran today
		 * @param variableFileContent
		 * @param variablesProperties
		 */
		private static void saveUnfollowedToday(Map<String, String> variableFileContent) {
			String now = LocalDateTime.now().toString();
			variableFileContent.put("hasUnfollowedToday", now);
			LocalDatabase.saveProperties(variableFileContent, Constants.FILE_VARIABLE_PATH);
		}

		/**
		 * Calculates how many users to follow and unfollow per run
		 * @param configFileContent
		 * @return
		 */
		private static int getUsersPerCicle(Map<String, String> configFileContent) {
			try {
				int secondsInBetweenPosts = Integer.valueOf(configFileContent.get(Constants.SECONDS_IN_BETWEEN_POSTS));
				int secondsInBetweenFollows = Integer.valueOf(configFileContent.get(Constants.SECONDS_IN_BETWEEN_FOLLOWS));
				return secondsInBetweenPosts/secondsInBetweenFollows/2;
			}
			catch (NumberFormatException e){
				Useful.printTimeStampedMessage(Constants.MESSAGE_DEFAULT_FOLLOW_NO_USERS);
				return 0;
			}
		}
		
		/**
		 * Prints message according to thrown exception
		 * @param e
		 * @param name
		 */
		private static void manageFollowExceptions(Exception e, String name) {
			if(e.getCause().getLocalizedMessage().equals(Constants.USER_NOT_FOUND)) {
				Useful.printTimeStampedMessageWAttributes(Constants.DELETE_USER_NOT_FOUND, Arrays.asList(name));
			}else {
				Useful.printTimeStampedMessage(Constants.EXCEPTION_IG_RESPONSE);
				e.printStackTrace();
			}
		}
		
		/**
		 * Prints message according to thrown exception
		 * @param e
		 * @param name
		 */		
		private static void manageUnfollowExceptions(Exception e, String name, Map<String, String> followsMap, int timeToSleep) {
			Useful.printTimeStampedMessage(Constants.EXCEPTION_IG_RESPONSE);
			e.printStackTrace();
			if(e.getLocalizedMessage().equals(Constants.USER_NOT_FOUND)) {
				Useful.printTimeStampedMessageWAttributes(Constants.DELETE_USER_NOT_FOUND, Arrays.asList(name));
				followsMap.remove(name);
				LocalDatabase.saveProperties(followsMap, Constants.FILE_FOLLOWS_PATH);
				Useful.sleep(2, Constants.WAIT);				
			}else if(e.getCause().getLocalizedMessage().equals(Constants.PLEASE_WAIT)) {
				e.printStackTrace();
				Useful.sleep(timeToSleep, Constants.EXCEPTION_IG_TRY_AGAIN_LATER);
			}
		}
}
