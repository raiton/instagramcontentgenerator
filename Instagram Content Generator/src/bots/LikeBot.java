package bots;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.github.instagram4j.instagram4j.IGClient;
import com.github.instagram4j.instagram4j.models.user.Profile;
import com.github.instagram4j.instagram4j.responses.IGResponse;
import com.github.instagram4j.instagram4j.actions.users.UserAction;
import com.github.instagram4j.instagram4j.actions.feed.FeedIterable;
import com.github.instagram4j.instagram4j.responses.users.UserResponse;

import database.LocalDatabase;
import externalapis.Instagram;

import com.github.instagram4j.instagram4j.models.friendships.Friendship;
import com.github.instagram4j.instagram4j.responses.feed.FeedUserResponse;
import com.github.instagram4j.instagram4j.responses.feed.FeedUsersResponse;
import com.github.instagram4j.instagram4j.models.media.timeline.TimelineMedia;
import com.github.instagram4j.instagram4j.requests.friendships.FriendshipsFeedsRequest;

import other.Constants;
import other.Useful;

public class LikeBot {

	public static void likeImages(IGClient client, Map<String, String> configFileContent, Map<String, String> variableFileContent) {
		int currentLikeCount = Integer.valueOf(variableFileContent.get("currentDailyLikes"));
		int maxDailyLikes = Integer.valueOf(configFileContent.get("maxDailyLikes"));
		if(currentLikeCount < maxDailyLikes) {
			likeUsersPosts(client, configFileContent, variableFileContent);
		}
		else {
			Useful.printTimeStampedMessageWAttributes(Constants.LIKED_MAX, Arrays.asList(maxDailyLikes));
		}
	}

	private static void likeUsersPosts(IGClient client, Map<String, String> configFileContent, Map<String, String> variableFileContent) {
		int maxDailyLikes = Integer.valueOf(configFileContent.get("maxDailyLikes"));
		int timeToSleep = Integer.valueOf(configFileContent.get("secondsInBetweenLikes"));
		int maxLikesPerPerson = Integer.valueOf(configFileContent.get("maxLikesPerPerson"));
		int currentDailyLikes = Integer.valueOf(variableFileContent.get("currentDailyLikes"));
		List<String> usersToLikeFrom = new ArrayList<>(Arrays.asList(configFileContent.get(Constants.ACCOUNTS_TO_FOLLOW_FROM).split(Constants.SPLITTER)));
		String waitingString = timeToSleep < 60 ? "Waiting for " + timeToSleep + " seconds..." : "Waiting for " + timeToSleep/60 + " minutes...";
		while(currentDailyLikes < maxDailyLikes) {
			boolean passou = false;
			for(String userToLikeFrom: usersToLikeFrom) {
				if(passou){
					break;
				}
				UserAction user = Instagram.getUser(client, userToLikeFrom);
				FeedIterable<FriendshipsFeedsRequest, FeedUsersResponse> followersFeed = user.followersFeed();
				for(Profile profile : followersFeed.iterator().next().getUsers()) {
					int currentLikeCount = 0;
					boolean notliked = false;
					String name = profile.getUsername();
					if(!profile.is_private()) {
						try {
							UserAction userToFollow = Instagram.getUser(client, name);
							
							Friendship friendshipWithUserToFollow = userToFollow.getFriendship().join();
							if(!friendshipWithUserToFollow.isFollowing() && !profile.is_private()) {		
								if(notliked) {
									Useful.sleep(2, Constants.WAIT);					
									notliked = false;
								}
								UserResponse usernameInfoRequest = Instagram.getUsernameInfo(client, profile);

								//Getting user feed by providing User pk
								FeedUserResponse userFeed = Instagram.getFeedUser(client, usernameInfoRequest); 
					
								//Iterating through user feed items
								List<TimelineMedia> mediaList = userFeed.getItems();
								for(TimelineMedia media: mediaList){
									if(currentLikeCount < maxLikesPerPerson) {
										if(!media.isHas_liked()) {
											//Get any details of the feed item.
											try {
												IGResponse response = Instagram.likeMedia(client, String.valueOf(media.getPk()));
												if(response.getStatusCode() == 200) {
													Useful.printTimeStampedMessageWAttributes(Constants.PHOTO_LIKED, Arrays.asList(profile.getUsername(), media.getCode()));
													notliked = false;
													currentLikeCount++;
													currentDailyLikes++;
													variableFileContent.put(Constants.CURRENT_DAILY_LIKES, String.valueOf(currentDailyLikes)); 
																					
													//Store locals
													LocalDatabase.saveProperties(variableFileContent, Constants.FILE_VARIABLE_PATH);
													Useful.sleep(timeToSleep, waitingString);
												}else {
													Useful.printTimeStampedMessage(Constants.AHH_SHIT + " MEGA ULTRA FAIL!");
												}
											}catch (InterruptedException e) {
												Useful.printTimeStampedMessage(Constants.EXCEPTION_INTERRUPTED);	
												e.printStackTrace();
											} catch (ExecutionException e) {
												Useful.printTimeStampedMessage(Constants.EXCEPTION_EXECUTION);		
												e.printStackTrace();
												Useful.sleep(91, Constants.WAIT);
											}catch (Exception e) {
												Useful.printTimeStampedMessage(Constants.AHH_SHIT + " <--------------------------------------------");		
												e.printStackTrace();
											}
										}	
										else{
											notliked = true;
										}
									}else {
										currentLikeCount = 0;
										break;
									}
								}
							}
						}catch(Exception e) {
							e.printStackTrace();
							Useful.sleep(timeToSleep, Constants.EXCEPTION_IG_TRY_AGAIN_LATER);
							passou = true;
							break;
						}
					}	
					else{
						notliked = true;
					}
				}
			}
			Useful.printTimeStampedMessage(Constants.NO_MORE_PEOPLE);
			break;
		}
	}
}
