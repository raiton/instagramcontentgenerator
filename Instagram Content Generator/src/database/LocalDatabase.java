package database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import other.Constants;
import other.Useful;

public class LocalDatabase {

	public static void createInitialConfigFile() {
		//Save settings Map in Local
		Map<String, String> map = new HashMap<>();
		map.put(Constants.BOT_TIME_STOP, "23:00");
		map.put(Constants.BOT_TIME_INITIATE, "10:30");
		map.put(Constants.SECONDS_IN_BETWEEN_FOLLOWS, "40");
		map.put(Constants.SECONDS_IN_BETWEEN_POSTS, "3600");
		map.put(Constants.MAX_DAILY_FOLLOW_UNFOLLOW, "200");
		map.put("reddit", "swapForYourSubreddit");
		map.put(Constants.USERNAME, "swapForYourUsername");
		map.put(Constants.PASSWORD, "swapForYourPassword");
		map.put("numberOfMemesToKeepInStorage", "20");
		map.put("maxDailyLikes", "700");
		map.put("maxLikesPerPerson", "3");
		map.put("secondsInBetweenLikes", "10");
		map.put("unfollowPeopleWhoHaventFollowedInXDays", "7");
		map.put(Constants.BOT_DM,Constants.YES);
		map.put(Constants.BOT_POST,Constants.NO);
		map.put(Constants.BOT_LIKE,Constants.YES);
		map.put(Constants.BOT_FOLLOW_UNFOLLOW,Constants.YES);
		map.put(Constants.ACCOUNTS_TO_FOLLOW_FROM, "9gag:basic.whitememes:kimkardashian:vancityreynolds");
		map.put(Constants.DUMMY_ACCOUNTS, "account1:account2:account3");
		map.put(Constants.DUMMY_PASSWORDS, "password1:password2:password3");
		
				
		saveProperties(map, Constants.DIRECTORY_CONFIG);
		Useful.printTimeStampedMessage(Constants.FIRST_RUN);
	}
	
	
	public static Map<String, String> createInitialVariableFile() {
		//Save settings Map in Local
		Map<String, String> variableFileContent = new HashMap<>();
		variableFileContent.put(Constants.CURRENT_DAILY_LIKES, Constants.ZERO);
		variableFileContent.put(Constants.HAS_UNFOLLOWED_TODAY, Constants.NO);
		variableFileContent.put(Constants.CURRENT_FOLLOW_UNFOLLOW_COUNT, Constants.ZERO);
		LocalDatabase.saveProperties(variableFileContent, Constants.FILE_VARIABLE_PATH);
		return variableFileContent;		
	}
	
	public static <T> void saveProperties(Map<String, T> propertiesFileContent, String fileDirectory) {
		ensureDirectories(); 
		Properties properties = new Properties();
		try {
			File propertiesFile = new File(fileDirectory);
			//Create file if it doesn't exist
			if(!propertiesFile.exists() && !propertiesFile.isDirectory()) {				
				propertiesFile.createNewFile();
			}
				for (Map.Entry<String,T> entry : propertiesFileContent.entrySet()) {
					properties.put(entry.getKey(), entry.getValue().toString());
			}
			
			// Store information in file
			properties.store(new FileOutputStream(fileDirectory), null);
		} catch (FileNotFoundException e) {
			Useful.printTimeStampedMessage(Constants.EXCEPTION_FILE_NOT_FOUND);
			e.printStackTrace();
		} catch (IOException e) {
			Useful.printTimeStampedMessage(Constants.EXCEPTION_IO);
			e.printStackTrace();
		}	
	}

	/**
	 * Loads the given property file and places all it's content in a map
	 * @param properties
	 * @param fileDirectory
	 */
	public static Map<String, String> readProperties(String fileDirectory) {
		Properties properties = new Properties();
		Map<String, String> propertiesFileContent = new HashMap<>();
		File followsFile = new File(fileDirectory);
		if(followsFile.exists() && !followsFile.isDirectory()) { 
			try {
				properties.load(new FileInputStream(fileDirectory));
			} catch (FileNotFoundException e) {
				Useful.printTimeStampedMessage(Constants.EXCEPTION_FILE_NOT_FOUND);
				e.printStackTrace();
			} catch (IOException e) {
				Useful.printTimeStampedMessage(Constants.EXCEPTION_IO);
				e.printStackTrace();
			}	
		
			for (String key : properties.stringPropertyNames()) {
				propertiesFileContent.put(key, properties.get(key).toString());
			}
		}
		return propertiesFileContent;
	}
	
	/**
	 * Loads the follow property file and places all it's content in a map
	 * @param properties
	 * @param fileDirectory
	 */
	public static Map<String, LocalDateTime> readFollowProperties() {
		Properties properties = new Properties();
		Map<String, LocalDateTime> propertiesFileContent = new HashMap<>();
		File followsPropertiesFile = new File(Constants.FILE_FOLLOWS_PATH);
		
		if(followsPropertiesFile.exists() && !followsPropertiesFile.isDirectory()) {
			try {
				properties.load(new FileInputStream(Constants.FILE_FOLLOWS_PATH));
			} catch (FileNotFoundException e) {
				Useful.printTimeStampedMessage(Constants.EXCEPTION_FILE_NOT_FOUND);
				e.printStackTrace();
			} catch (IOException e) {
				Useful.printTimeStampedMessage(Constants.EXCEPTION_IO);
				e.printStackTrace();
			}
			for (String key : properties.stringPropertyNames()) {
				if(!properties.get(key).equals(Constants.UNFOLLOWED) && !properties.get(key).equals(Constants.FOLLOWED_BACK)) {
					propertiesFileContent.put(key, LocalDateTime.parse((CharSequence) properties.get(key)));
				}
			}
		}
		else {
			try {
				followsPropertiesFile.createNewFile();
			} catch (IOException e) {
				Useful.printTimeStampedMessage(Constants.EXCEPTION_IO);
				e.printStackTrace();						 
			}
		}
		return propertiesFileContent;
	}
	
	/**
	 * Creates main directories if they don't exist
	 */
	private static void ensureDirectories() {
		File configDirectory = new File(Constants.DIRECTORY_CONFIG);
		File contentDirectory = new File(Constants.DIRECTORY_MEMES);
		 if (!configDirectory.exists()){
			 configDirectory.mkdir();
		 }
		 if (!contentDirectory.exists()){
			 contentDirectory.mkdir();
		 }
	}
}
