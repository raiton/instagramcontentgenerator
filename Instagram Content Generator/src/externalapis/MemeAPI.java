package externalapis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import other.Constants;
import other.Useful;

public class MemeAPI {

	private static HttpURLConnection connection;
	
	public static String getMemes(int numberOfMemes) {
		BufferedReader reader = null;
		String line;
		StringBuffer responseContent = new StringBuffer();
		List<String> list = new ArrayList<>();		
		try {
			URL url = new URL("https://meme-api.herokuapp.com/gimme/" + numberOfMemes);
			connection = (HttpURLConnection) url.openConnection();
				
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			int status = connection.getResponseCode();
				
			if(status > 299) {
				reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				while((line = reader.readLine()) != null) {
					responseContent.append(line);
					list.add(line);	
				}
				reader.close();
			}else {
				reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				while((line = reader.readLine()) != null) {
					responseContent.append(line);
					list.add(line);	
				}
				reader.close();
			}
		}catch (MalformedURLException e) {
			Useful.printTimeStampedMessage(Constants.EXCEPTION_MALFORMED_URL);
			e.printStackTrace();
		} catch (ProtocolException e) {
			Useful.printTimeStampedMessage(Constants.EXCEPTION_PROTOCOL);
			e.printStackTrace();
		} catch (IOException e) {
			Useful.printTimeStampedMessage(Constants.EXCEPTION_IO);
			e.printStackTrace();
		}finally {
			connection.disconnect();
		}
		return responseContent.toString();
		
	}

}
