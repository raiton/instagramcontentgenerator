package externalapis;

import java.io.File;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.github.instagram4j.instagram4j.IGClient;
import com.github.instagram4j.instagram4j.actions.feed.FeedIterable;
import com.github.instagram4j.instagram4j.actions.users.UserAction;
import com.github.instagram4j.instagram4j.exceptions.IGLoginException;
import com.github.instagram4j.instagram4j.models.user.Profile;
import com.github.instagram4j.instagram4j.requests.feed.FeedUserRequest;
import com.github.instagram4j.instagram4j.requests.friendships.FriendshipsFeedsRequest;
import com.github.instagram4j.instagram4j.requests.friendships.FriendshipsActionRequest.FriendshipsAction;
import com.github.instagram4j.instagram4j.requests.media.MediaActionRequest;
import com.github.instagram4j.instagram4j.requests.media.MediaActionRequest.MediaAction;
import com.github.instagram4j.instagram4j.requests.users.UsersUsernameInfoRequest;
import com.github.instagram4j.instagram4j.responses.IGResponse;
import com.github.instagram4j.instagram4j.responses.feed.FeedUserResponse;
import com.github.instagram4j.instagram4j.responses.feed.FeedUsersResponse;
import com.github.instagram4j.instagram4j.responses.users.UserResponse;

import other.Constants;
import other.Useful;

public class Instagram {

	/**
	 * Logins into Instagram using the config file
	 * @param configFileContent
	 * @return
	 * @throws IGLoginException
	 */
	public static IGClient login(Map<String, String> configFileContent) throws IGLoginException {
		return login(configFileContent.get(Constants.USERNAME), configFileContent.get(Constants.PASSWORD));
	}
	
	/**
	 * Logins into Instagram using an username and password
	 * @param configFileContent
	 * @return
	 * @throws IGLoginException
	 */
	public static IGClient login(String username, String password) throws IGLoginException {
		return IGClient.builder().username(username).password(password).login();
	}
	
	/**
	 * Uploads photo to instagram
	 * @param client
	 * @param fileToUpload
	 */
	public static void uploadPhoto(IGClient client, File fileToUpload) {
		client.actions().timeline().uploadPhoto(fileToUpload, Useful.getFileName(fileToUpload) + "\n\n\n#meme #memes #memesdaily #memestagram #memepage #memelord #memeoftheday #memeita #memesfordays #memesita #memeaccount #memesaremee #memer #memecucks #memegod #memeindonesia #memeinajah #memesespa #memedaily #memelife #memez #memecomicindonesia #memeitalia #memestar #memesenespa #memelucu #memester #memevideo #memequeen #memes4ever")  
		.thenAccept(res -> {
			// perform actions with response
			Useful.printTimeStampedMessage("Uploaded photo " + res.getMedia().getId());
				    
			if(fileToUpload.delete()) {
				Useful.printTimeStampedMessage(Constants.PHOTO_DELETE_SUCCESS);
			}
			else{
				Useful.printTimeStampedMessage(Constants.PHOTO_DELETE_FAIL);
			}
		})
		.exceptionally(tr -> {
			// something has gone terribly wrong!
			Useful.printTimeStampedMessage(tr.toString());
			return null;
		}).join();
	}

	/**
	 * Gets Instagram User from username
	 * @param client
	 * @param user
	 * @return
	 */
	public static UserAction getUser(IGClient client, String user) {
		return client.actions().users().findByUsername(user).join();
	}

	public static void sendMessage(IGClient client, Map<String, String> configFileContent) {
//		client.actions()
	}
	
	public static IGResponse likeMedia(IGClient client, String privateKey) throws InterruptedException, ExecutionException {
		MediaActionRequest request = new MediaActionRequest(privateKey, MediaAction.LIKE);
		request.formRequest(client);
		return request.execute(client).get();
	}

	public static UserResponse getUsernameInfo(IGClient client, Profile profile) {
		return new UsersUsernameInfoRequest(profile.getUsername()).execute(client).join();
	}

	public static FeedUserResponse getFeedUser(IGClient client, UserResponse usernameInfoRequest) {
		return new FeedUserRequest(usernameInfoRequest.getUser().getPk()).execute(client).join();
	}

	public static List<Profile> getFollowersFromUser(IGClient client, String userToFollowFrom, int numberOfUsersToFollow) {
		UserAction user = getUser(client, userToFollowFrom);			    
		FeedIterable<FriendshipsFeedsRequest, FeedUsersResponse> followersFeed = user.followersFeed();		
		
		//Followers 
		List<Profile> followerList = new ArrayList<>();
		for(FeedUsersResponse page : followersFeed) {
			followerList.addAll(page.getUsers());
			if(followerList.size() > 5*numberOfUsersToFollow) {
				break;
			}
		}
		return followerList;
	}

	/**
	 * Will follow a given user
	 * @param userToFollow
	 */
	public static void follow(UserAction userToFollow) {
		userToFollow.action(FriendshipsAction.CREATE).join();
	}

	/**
	 * Will unfollow a given user
	 * @param userToUnfollow
	 */
	public static void unfollow(UserAction userToUnfollow) {
		userToUnfollow.action(FriendshipsAction.DESTROY).join();
		
	}
}
